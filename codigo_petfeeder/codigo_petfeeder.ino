#include <ESP8266WiFi.h>
#include <NTPClient.h>         //librería del cliente NTP
#include <WiFiUdp.h>           //librería UDP para comunicar con  NTP
#include <Servo.h>
#include <UniversalTelegramBot.h>
#include <WiFiClientSecure.h>
#include <Ticker.h>
#include <EEPROM.h>
#include <ArduinoOTA.h>
#include <ESP8266mDNS.h>

Servo servo;
WiFiUDP ntpUDP;               //iniciamos el cliente udp para su uso con el server NTP

//WiFiServer server(80);

Ticker timer_ISR;

#define time_refresh    300000              // tasa de refresco en ms
#define pos_servo_max   100
#define pos_servo_min   30
#define cant_entregas   10
#define raciones        2
#define LONGBUTTON      200

#define enable_servo    0
#define servo_pin       2
#define led             4
#define button          5

#define BOTtoken "1204756896:AAGd6bKHIbkTPfnQpnKw2_7zdpNllhOVe7M"  // el token de tu BOT, lo obtenemos de BotFather
#define mi_chat_id "1296864907"

// departamento
const char* ssid = "Speedy-Fibra-8F8D64";//type your ssid
const char* password = "39919131";//type your password

// casa
//const char* ssid = "wifiklapp";//type your ssid
//const char* password = "0141760314";//type your password

unsigned long int mainloop_counter=0,ant_millis=0;
bool flag_led=0,flag_entregas=0,entregar_time1=0,entregar_time2=0,flag_aviso=0,flag_aviso1=0,flag_aviso2=0,flag_avisoSusp=0,flag_avisoButton=0;
bool press_button=0,aviso_longbutton=0,valid_time=0,flag_connect=0,flag_suspend=0,butt_ant=0,flag_notif=1;
unsigned int dia,horas,minutos,segundos,ms,set_time1[2],set_time2[2],set_aviso[3],ult_entrega[5],cont_entregas=0;

// cuando creamos el cliente NTP podemos especificar el servidor al que nos vamos a conectar en este caso
// 0.south-america.pool.ntp.org SudAmerica
// también podemos especificar el offset en segundos para que nos 
// muestre la hora según nuestra zona horaria en este caso
// restamos -10800 segundos ya que estoy en argentina. 
// y por ultimo especificamos el intervalo de actualización en
// mili segundos en este caso 6000
NTPClient timeClient(ntpUDP, "0.south-america.pool.ntp.org",-10800,time_refresh);

WiFiClientSecure client;
UniversalTelegramBot bot(BOTtoken, client);

bool Start = false;

void entregar_comida(){
int i;
  digitalWrite(led, HIGH);
  digitalWrite(enable_servo,HIGH);

  ult_entrega[0]=horas;
  ult_entrega[1]=minutos;
  ult_entrega[2]=segundos;
  ult_entrega[3]=dia;
  
  for(i=0;i<raciones;i++){
    servo.write(pos_servo_max);
    delay(300);
    servo.write(pos_servo_min);
    delay(300);
  }
  digitalWrite(enable_servo,LOW);
  digitalWrite(led, LOW);
  cont_entregas++;
  if(cont_entregas>cant_entregas){
    flag_entregas=1;
  }
  EEPROM.write(0,cont_entregas);
  EEPROM.write(16,ult_entrega[0]);
  EEPROM.write(17,ult_entrega[1]);
  EEPROM.write(18,ult_entrega[2]);
  EEPROM.write(19,ult_entrega[3]);
  EEPROM.commit();    //Store data to EEPROM  
}


void reloj(){
  if(segundos<59){
    segundos++;
  }
  else{
    segundos=0;
    if(minutos<59){
      minutos++;
    }
    else{
      minutos=0;
      if(horas<24){
        horas++;
      }
      else{
        horas=0;
      }
    }
  }
  
  if((set_time1[0]==1)&&(horas==set_time1[1])&&(minutos==0)&&(segundos==0)){
    entregar_time1=1;
  }
  if((set_time2[0]==1)&&(horas==set_time2[1])&&(minutos==0)&&(segundos==0)){
    entregar_time2=1;
  }
  
  if((cant_entregas<=cont_entregas) && (horas==set_aviso[0])&&(minutos==0)&&(segundos==0)){
    flag_aviso=1;
  }
  
//  Serial.print(horas);
//  Serial.print(":");
//  Serial.print(minutos);
//  Serial.print(":");
//  Serial.println(segundos);
}

void enviarNotif(String msj){

  if(flag_notif){
    bot.sendMessage(mi_chat_id, msj , "");
  }

}

void estado(){

  cont_entregas=EEPROM.read(0x00);

  for(int i=0;i<2;i++){
    set_time1[i]=EEPROM.read(i+1);
    set_time2[i]=EEPROM.read(i+5);  
  }

  Serial.println(String(EEPROM.read(1)) +" - "+String(EEPROM.read(2)) +" - "+String(EEPROM.read(5)) +" - "+String(EEPROM.read(6)) );

  for(int i=0;i<4;i++){
    if(i<3){
      set_aviso[i]=EEPROM.read(i+10);
    }
    ult_entrega[i]=EEPROM.read(i+16);
    
  }

  String msj="PetFeeder conectado.\n";

  if(valid_time==0){                                              // si el horario no se actualizo todavia
    msj+="(Actualizando horario)...";
  }
  else{
    msj+= "Horario actual: "+String(horas)+":"+ String(minutos)+":"+String(segundos);
  }

  msj+= "\nLos horarios son:\nHorario 1: ";
  if(set_time1[0]==1){
    msj+= "habilitado: "+String(set_time1[1])+"hs\n";
  }
  else{
    msj+= "deshabilitado.\n";
  }
  msj+= "Horario 2: ";
  if(set_time2[0]==1){
    msj+= "habilitado: "+String(set_time2[1])+"hs\n";
  }
  else{
    msj+= "deshabilitado.\n";
  }
  msj+="Horario de notificación: "+String(set_aviso[0])+"hs\n";

  if(cant_entregas>=cont_entregas){
    msj+="Quedan: "+String(cant_entregas-cont_entregas)+" entregas.\n";
  }
  else{
    msj+="No quedan entregas disponibles\n";
  }
  msj+= "Última entrega realizada ";
  if(ult_entrega[3]!= timeClient.getDay()){
    switch(ult_entrega[3]){
      case 0:
            msj+="el domingo";
      break;
      case 1:
            msj+="el lunes";
      break;
      case 2:
            msj+="el martes";
      break;
      case 3:
            msj+="el miércoles";
      break;
      case 4:
            msj+="el jueves";
      break;
      case 5:
            msj+="el viernes";
      break;
      case 6:
            msj+="el sábado";
      break;
    }
  }
  else{
    msj+="hoy";
  }
  msj+=" a las: "+String(ult_entrega[0])+":"+String(ult_entrega[1])+":"+String(ult_entrega[2]);
  Serial.println(msj); 

  enviarNotif(msj);
  //bot.sendMessage(mi_chat_id, msj , "");
}

void handleNewMessages(int numNewMessages){
 
  for(int i=0;i<numNewMessages;i++){
    String chat_id = String(bot.messages[i].chat_id);
    String text = bot.messages[i].text;
    String from_name = bot.messages[i].from_name;
    if(chat_id == mi_chat_id){
      if(from_name == ""){
        from_name="Guest";
      }
        
      Serial.println("de: "+from_name);
     
      if(text == "/balanceado"){
        entregar_comida();
        enviarNotif("acabo de darle balancearo a Dexter via TL!");
      }
      else if(text == "/enable1"){      
        if(EEPROM.read(1)==0){
          EEPROM.write(1, 1);  
          EEPROM.commit();    //Store data to EEPROM
        }
        enviarNotif("Horario 1 habilitado");
      }
      else if(text == "/disable1"){ 
        if(EEPROM.read(1)==1){
          EEPROM.write(1, 0);  
          EEPROM.commit();    //Store data to EEPROM
        }     
        enviarNotif("Horario 1 deshabilitado");
      }
      else if(text == "/enable2"){
        if(EEPROM.read(5)==0){
          EEPROM.write(5, 1);  
          EEPROM.commit();    //Store data to EEPROM
        }      
        enviarNotif("Horario 2 habilitado");
      }
      else if(text == "/disable2"){ 
        if(EEPROM.read(5)==1){
          EEPROM.write(5, 0);  
          EEPROM.commit();    //Store data to EEPROM
        }      
        enviarNotif("Horario 2 deshabilitado");
      }
      else if(text == "/suspend"){                                            //suspende la proxima entrega      
        flag_suspend=1;
        enviarNotif("Próxima entrega suspendida");
      }  
      else if(text == "/cancelsuspend"){                                      //cancela la suspension     
        flag_suspend=0;
        enviarNotif("Suspensión de entregas cancelado");
      } 
      else if(text == "/activarnotif"){                                            //suspende la proxima entrega      
        flag_notif=1;
        enviarNotif("Notificaciones activadas");
      }  
      else if(text == "/desactivarnotif"){                                      //cancela la suspension     
        flag_notif=0;
        enviarNotif("Se desactivaran las notificaciones");
      } 
      else if(text == "/estado"){      
        estado();
      }
      else if(text == "/localip"){  
        enviarNotif("\nIP local: "+ WiFi.localIP().toString());
      }
      else if(text == "/reset"){ 
        if(EEPROM.read(0)>0){
          EEPROM.write(0, 0);
          cont_entregas=0;  
          EEPROM.commit();    //Store data to EEPROM
        }      
        enviarNotif("Contador de entregas reseteado");
      }
      else if(text == "/start"){      //Es el mensaje que recibimos en el celular cuando conectamos
        String welcome = "Bienvenido a PetFeeder-bot, " + from_name + ".\n";
        welcome += "Puede usar los siguientes comandos:\n\n";
        welcome += "/balanceado -> Para entregar una racion\n";
        welcome += "/enableX -> Para habilitar el horario X\n";
        welcome += "/disableX -> Para deshabilitar el horario X\n";
        welcome += "/horarioX: XXhs -> Para  configurar el horario\n";
        welcome += "/suspend -> Suspende la entrega siguiente,luego reanuda entregas normal\n";
        welcome += "/cancelsuspend -> Cancela la suspension de entregas\n";
   
        welcome += "/activarnotif -> Activa notificaciones\n";
        welcome += "/desactivarnotif -> desactiva las notificaciones\n";

 
        
        welcome += "/reset -> Para resetear el contador una vez llenado el recipiente\n";
        welcome += "/estado -> Devuelve el estado actual del petfeeder\n";
        welcome += "/localip -> Devuelve ip local\n";

        enviarNotif(welcome);

        const String commands = F("["
                            "{\"command\":\"balanceado\",   \"description\":  \"Para entregar una racion\"},"
                            "{\"command\":\"suspend\",    \"description\":  \"Suspende la entrega siguiente\"},"
                            "{\"command\":\"cancelsuspend\",    \"description\":  \"Cancela la suspension de entregas\"},"
                            
                            "{\"command\":\"activarnotif\",    \"description\":  \"Activa notificaciones\"},"
                            "{\"command\":\"desactiarnotif\",    \"description\":  \"Desactiva notificaciones\"},"
                            
                            "{\"command\":\"reset\",      \"description\":  \"Para resetear el contador una vez llenado el recipiente\"},"
                            "{\"command\":\"estado\",     \"description\":  \"Devuelve el estado actual del petfeeder\"},"
                            "{\"command\":\"localip\",    \"description\":  \"Devuelve ip local\"}"                //no lleva coma el ultimo comando
                            "]");
        bot.setMyCommands(commands);                    //setea todos los comandos antes mencionados en el menu de opciones del chat, es invisible para el usuario    
      }
      
      else if(text.indexOf("/horario1: ")!= -1){

        int checksum=text.indexOf("hs");                                        //find "hs" position into string
        if(checksum > 11 && checksum <= 13){                                   
          String val=text.substring(11,checksum);
          int val_num=val.toInt();
          
          if(val_num>0&&val_num<24){                                            //if hour it is beetween 0 to 23hs
            EEPROM.write(1, 1);                                                 //enable hour 1 
            EEPROM.write(2, val_num); 
            EEPROM.commit();    //Store data to EEPROM
            Serial.println("Horario 1 configurado a las " +val+ "hs.");

            enviarNotif("Horario 1 configurado a las " +val+ "hs.");
          }
          else{
            Serial.println("ERROR CONFIGURACION HORARIO");
            enviarNotif("Error configurando horario deshabilitado");
            EEPROM.write(1, 0);                                                 // disable hour 1
            EEPROM.commit();                                                    //Store data to EEPROM
          }
        }
        else{
          Serial.println("ERROR CONFIGURACION HORARIO");
          enviarNotif("Error configurando horario deshabilitado");
          EEPROM.write(1, 0);                                                 // disable hour 1
          EEPROM.commit();                                                    //Store data to EEPROM
        }
      }
      
      else if(text.indexOf("/horario2: ")!= -1){

        int checksum=text.indexOf("hs");                                        //find "hs" position into string
        if(checksum > 11 && checksum <= 13){                                   
          String val=text.substring(11,checksum);
          int val_num=val.toInt();
          
          if(val_num>0&&val_num<24){                                            //if hour it is beetween 0 to 23hs
            EEPROM.write(5, 1);                                                 //enable hour 2 
            EEPROM.write(6, val_num); 
            EEPROM.commit();    //Store data to EEPROM
            Serial.println("Horario 2 configurado a las " +val+ "hs.");
            enviarNotif("Horario 2 configurado a las " +val+ "hs.");
          }
          else{
            Serial.println("ERROR CONFIGURACION HORARIO");
            enviarNotif("Error configurando horario deshabilitado");
            EEPROM.write(5, 0);                                                 // disable hour 1
            EEPROM.commit();                                                    //Store data to EEPROM
          }
        }
        else{
          Serial.println("ERROR CONFIGURACION HORARIO");
          enviarNotif("Error configurando horario deshabilitado");
          EEPROM.write(5, 0);                                                 // disable hour 1
          EEPROM.commit();                                                    //Store data to EEPROM
        }
      }
      else{
        enviarNotif("Comando erroneo: \""+text+"\"");
      }
    }
    else{
        enviarNotif("No te conozco");     
    }
  }
}

ICACHE_RAM_ATTR void button_irq(){                                //IRQ boton button
  press_button=1;
}
 
void setup(){
  int i;
 
  Serial.begin(115200);
  EEPROM.begin(512);
  
  pinMode(enable_servo, OUTPUT);
  pinMode(led, OUTPUT);
  pinMode(button, INPUT_PULLUP);
  digitalWrite(enable_servo, LOW);
  digitalWrite(led, LOW);

  servo.attach(servo_pin); 
  servo.write(pos_servo_min);

  attachInterrupt(digitalPinToInterrupt(button), button_irq, FALLING);
   
  Serial.print("Conectando a: ");
  Serial.println(ssid);

  flag_connect=0;
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  for(i=0;i<5;i++){ 
    digitalWrite(led,HIGH);
    delay(50);
    digitalWrite(led,LOW);
    delay(50); 
  }

  if(EEPROM.read(0)==255 ||EEPROM.read(5)==255||EEPROM.read(15)==255){            // si la eeprom esta vacia
    for(int i=0;i<20;i++){
      EEPROM.write(i,0);                                                          //la cargo con 0
    }
    EEPROM.commit();    //Store data to EEPROM  
    Serial.println("EEPROM INICIALIZADA.");
  }
  
  EEPROM.write(10,20);
  EEPROM.write(11,0);
  EEPROM.write(12,0);
  EEPROM.commit();    //Store data to EEPROM  
}

int cont=0;
void loop(){
  unsigned long buffer_millis=0,duration=0;
  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////Servicio que corren con o sin internet///////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  if(press_button){                                                                         //button event flag
    duration=pulseIn(button,LOW)/1000;                                                      //convierto en ms
    if(duration==0 || duration>=LONGBUTTON){                                                //si duration=0 se vencio el timeout se pulso por mas de 1s  
        EEPROM.write(0, 0);
        cont_entregas=0;  
        EEPROM.commit();    //Store data to EEPROM
        aviso_longbutton=1;
        press_button=0;
    }
    else{
      entregar_comida();
      flag_avisoButton=1;
      press_button=0;
    }
  }
  if(mainloop_counter%500 ==0){ 
    digitalWrite(led,flag_led);
    flag_led=!flag_led;
  }
  
  if(valid_time){                                                                                       //if timer run valid time attend food routines
     if(entregar_time1){                                                                                //give food time1
      if(flag_suspend==0){
        entregar_comida();
        flag_aviso1=1;
        entregar_time1=0;
      }
      else{
        flag_suspend=0;
        flag_avisoSusp=1;
        entregar_time1=0;
      }
    }
    
    if(entregar_time2){                                                                                  //give food time2
      if(flag_suspend==0){
        entregar_comida();
        flag_aviso2=1;
        entregar_time2=0;
      }
      else{
        flag_suspend=0;
        flag_avisoSusp=1;
        entregar_time2=0;
      }
    }
  }

  if(flag_connect==0){
    if(WiFi.status() == WL_CONNECTED){                                                     //cuando no estaba conectado y conecto por primera vez
        Serial.println("\nWiFi connected");
        client.setInsecure();                                                               //no se para que es
        enviarNotif("Petfeeder Iniciando..");  
        timeClient.begin();     // inicio servicio ntp
        ArduinoOTA.begin();     // inicio servicio OTA
        estado();
        Serial.print("Direccion ip local: ");
        Serial.print(WiFi.localIP());
        flag_connect=1;
    }
  }
  else{                                                                                     //Servicio que necesita internet para correr
    if(mainloop_counter%3000==0){                                                          //read telegram bot 
      int numNewMessages = bot.getUpdates(bot.last_message_received + 1);
      if(numNewMessages>0){                                                                 // Mensaje recibido en Telegram
        handleNewMessages(numNewMessages);
        numNewMessages = bot.getUpdates(bot.last_message_received + 1);
        Serial.println("MENSAJE RECIBIDO DE TL");
      }  
    } 
    if((mainloop_counter%time_refresh) ==0 || valid_time==0){                                                 //sincronize hour with ntp protocol every "time refresh"
      timeClient.update(); //sincronizamos con el server NTP
      dia=timeClient.getDay();
      horas=timeClient.getHours();
      minutos=timeClient.getMinutes();
      segundos=timeClient.getSeconds();
      
      if(valid_time==0){                                                                    //if valid_time is 0,then run timer clock
        timer_ISR.attach(1, reloj);
        valid_time=1;
      }
    }
    
    if(flag_aviso){                                                                      //if the petFedder give all the food  times,we need fill the deposit
      enviarNotif("Rellenar de balancearo el depósito");
      flag_aviso=0;
    }

    if(flag_aviso1){                                                                                    //notifico sobre la segunda entrega                                                                      
      if(cant_entregas<=cont_entregas){                                                                 //si se pasaron las entregas                                                   
          enviarNotif("horario 1: Rellenar de balanceado el depósito");   
      }
      else{
          enviarNotif("acabo de darle balancearo a Dexter horario 1");
      }
      flag_aviso1=0;
    }

    if(flag_aviso2){                                                                                    //notifico sobre la segunda entrega
      if(cant_entregas<=cont_entregas){                                                                 //si se pasaron las entregas 
        enviarNotif("horario 2: Rellenar de balanceado el depósito");   
      }
      else{
        enviarNotif("acabo de darle balancearo a Dexter horario 2");
      }
      flag_aviso2=0;
    }

    if(flag_avisoSusp){  
      enviarNotif("horario suspendido, se reanudan las entregas"); 
      flag_avisoSusp=0;
    }
    if(flag_avisoButton){
        enviarNotif("entregue balanceado via button");
        flag_avisoButton=0;
    }
    if(aviso_longbutton){
        enviarNotif("Contador de entregas reseteado via button");
        aviso_longbutton=0;
    }

    ArduinoOTA.handle();                    // handler of ota services
  }
  mainloop_counter++;
  delay(1);
}
